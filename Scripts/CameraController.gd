extends Camera

var target : Node = null

func _process(delta):
	target = get_node("/root/Prototype/GameManager").player
	if target != null:
		var offset = (target.transform.origin - transform.origin) * 0.1
		transform.origin += offset + Vector3(0, 2, 2)