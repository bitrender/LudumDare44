extends Control

func update_result(result : Dictionary):
	$Panel/VBoxContainer/PaymentBox/VBoxContainer/Line01/HBoxContainer/Edit.text = String(result.get('rescued'))
	$Panel/VBoxContainer/PaymentBox/VBoxContainer/Line02/HBoxContainer/Edit.text = String(result.get('alive'))
	$Panel/VBoxContainer/PaymentBox/VBoxContainer/Line03/HBoxContainer/Edit.text = String(result.get('dead'))
	$Panel/VBoxContainer/PaymentBox/VBoxContainer/Line04/HBoxContainer/Edit.text = "$ " + String(result.get('price'))
	$Panel/VBoxContainer/PaymentBox/VBoxContainer/Line05/HBoxContainer/Edit.text = "$ " + String(result.get('total'))