extends Control

var index : int = 0
var messages : Array

func _ready():
	messages.append("This city needs your courage to defend the local citizens...")
	messages.append("This monster will be devour the people of the city...")
	messages.append("Take everyone to a safe place. (press E to call a citizen)")

func next_message():
	$Panel/Label.text = messages[index]
	index += 1