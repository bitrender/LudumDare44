extends KinematicBody

var speed = 200
var _nav : Navigation
var target_point : Vector3
var moving_direction : Vector3
var calculated_path : PoolVector3Array
var grasped : bool = false
var movement : Vector3
var moving : bool = false
var following_node : Node

func _ready():
	_nav = get_node("/root/Prototype/GameManager/GameObjects/City/Navigation-Hostage")

func _physics_process(delta):
	if grasped:
		speed = 800
		moving = false
		var direction = get_global_transform().origin.direction_to(following_node.get_global_transform().origin)
		move_and_slide(direction * delta * speed)
		look_at(get_global_transform().origin - direction, Vector3(0, 1, 0))
	else:
		speed = 200
		if moving:
			idle_movement(delta)
		else:
			calculated_path = _nav.get_simple_path(transform.origin, transform.origin + get_random_position())
			moving = true

func get_random_position():
	randomize()
	return Vector3(rand_range(-10, 10), 0, rand_range(-10, 10))

func idle_movement(delta):
	if calculated_path.size() > 0:
		if transform.origin.distance_to(calculated_path[0]) > 10:
			moving_direction = transform.origin.direction_to(calculated_path[0])
			move_and_slide(moving_direction * delta * speed)
			look_at(get_global_transform().origin - moving_direction, Vector3(0, 1, 0))
		else:
			calculated_path.remove(0)
	else:
		moving = false
	
func rescue():
	$Mesh.visible = false
	$CollisionShape.disabled = true
	$AudioStreamPlayer3D.play()
	yield($AudioStreamPlayer3D, "finished")
	queue_free()