extends KinematicBody

var movement : Vector3
var speed : int = 800
var flying : bool = false
var close_object : Node = null
var grasped_hostage : KinematicBody = null
	
func _process(delta):
	if Input.is_action_pressed('game_down'):
		movement.z = 1
	if Input.is_action_pressed('game_up'):
		movement.z = -1
	if Input.is_action_pressed('game_right'):
		movement.x = 1
	if Input.is_action_pressed('game_left'):
		movement.x = -1
	#if Input.is_action_pressed('game_jump'):
		#movement.y = 1
	if Input.is_action_just_pressed('game_action'):
		grab_hostage()

func _physics_process(delta):
	movement.y -= 0.2
	
	#if movement.x + movement.y < 0.1:
	#	$AnimationTree.set("parameters/conditions/idle", true) 
	
	# change movement to rotation
	#rotate_y(-movement.x * delta)
	#movement.x = 0
	#move_and_slide(global_transform.basis.z * movement.z * delta * speed)
	
	# original movement
	move_and_slide(movement * delta * speed)
	look_at(transform.origin - movement, Vector3(0, 1, 0))
		
	movement = Vector3()
	
	#grasped_hostage
	if grasped_hostage != null:
		var wr = weakref(grasped_hostage)
		if wr.get_ref():
			grasped_hostage.following_node = self
			pass
		else:
			grasped_hostage = null

func grab_hostage():
	if grasped_hostage == null:
		if weakref(close_object).get_ref():
			if close_object.is_in_group('hostages'):
				var hostage_distance = close_object.get_global_transform().origin.distance_to(get_global_transform().origin)
				if hostage_distance < 3:
					grasped_hostage = close_object
					grasped_hostage.grasped = true
					$AudioStreamPlayer3D.play()
	else:
		grasped_hostage.grasped = false
		grasped_hostage = null

func _on_GrabArea_body_entered(body):
	print(body.name)
	close_object = body
