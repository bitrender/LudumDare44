extends Node

var hostages : Array
var rescued_hostages : int = 0
var dead_hostages : int = 0
var _nav : Navigation
var game_ended : bool = false
var result : Dictionary
var player : Node
var is_music_on : bool = true
var is_sfx_on : bool = true
var game_started : bool = false

func _ready():
	$GameObjects/DynamicObjects.get_tree().paused = true
	initial_sequence()
	player = $GameObjects/DynamicObjects/SuperHero
	hostages = get_hostages()

func initial_sequence():
	$AnimationPlayer.play("InitialSequence")

func start_game():
	game_started = true
	$GameObjects/DynamicObjects.get_tree().paused = false

func _process(delta):
	if !game_ended:
		update_ui()
	if $AnimationPlayer.is_playing():
		if Input.is_action_just_pressed("ui_accept"):
			$AnimationPlayer.stop()
			start_game()
			$GameUI/HUD.visible = true
			$GameUI/Screen/Initial.visible = false
	
func _physics_process(delta):
	if !game_ended:
		if $GameObjects/DynamicObjects/Hostages.get_child_count() < 1:
			game_over()
	
func update_ui():
	$GameUI/HUD/TimePanel/Edit.text = String(int($Timer.time_left))
	$GameUI/HUD/HostagesPanel/Edit.text = String($GameObjects/DynamicObjects/Hostages.get_child_count())
	$GameUI/HUD/RescuedHostagesPanel/Edit.text = String(rescued_hostages)

func get_hostages():
	return $GameObjects/DynamicObjects/Hostages.get_children()

func game_over():
	game_ended = true
	$GameObjects.get_tree().paused = true
	calculate_points()
	$GameUI/Screen/Final.update_result(result)
	$GameUI/Screen/Final.visible = true

func restart():
	get_tree().reload_current_scene()

func music_trigger():
	if is_music_on:
		is_music_on = false
		$GameUI/Screen/Pause/Panel/VBoxContainer/ButtonsBox/VBoxContainer/MusicButton/Label.text = "Music OFF"
		AudioServer.set_bus_mute(2, true)
	else:
		is_music_on = true
		$GameUI/Screen/Pause/Panel/VBoxContainer/ButtonsBox/VBoxContainer/MusicButton/Label.text = "Music ON"
		AudioServer.set_bus_mute(2, false)
		
func sfx_trigger():
	if is_sfx_on:
		is_sfx_on = false
		$GameUI/Screen/Pause/Panel/VBoxContainer/ButtonsBox/VBoxContainer/SFXButton/Label.text = "SFX OFF"
		AudioServer.set_bus_mute(1, true)
	else:
		is_sfx_on = true
		$GameUI/Screen/Pause/Panel/VBoxContainer/ButtonsBox/VBoxContainer/SFXButton/Label.text = "SFX ON"
		AudioServer.set_bus_mute(1, false)

func calculate_points():
	result = Dictionary()
	result['rescued'] = rescued_hostages
	result['alive'] = $GameObjects/DynamicObjects/Hostages.get_child_count()
	result['dead'] = dead_hostages
	result['price'] = 100
	result['total'] = (result['rescued'] * result['price']) + (result['dead'] * -result['price'])
	
	print(result.get('total'))

func _on_SafeArea_body_entered(body):
	print('ssad')
	if body.is_in_group('hostages'):
		rescued_hostages += 1
		body.rescue()

func main_menu():
	get_tree().change_scene("res://Scenes/MainMenu.tscn")
