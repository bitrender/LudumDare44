extends KinematicBody

var movement : Vector3
var speed : int = 5
var hostages : Array
var closest_hostage : Node = null
var _nav : Navigation
var hunting : bool = false
var calculated_path : Array

func _ready():
	_nav = get_node("/root/Prototype/GameManager/GameObjects/City/Navigation-Monster")

func _physics_process(delta):
	follow_closest_hostage(delta)
	pass

func follow_closest_hostage(delta):
	get_closest_hostage()
	if closest_hostage != null:
		if !hunting:
			calculated_path = _nav.get_simple_path(transform.origin, closest_hostage.get_global_transform().origin)
			hunting = true
		else: 
			if calculated_path.size() > 0:
				calculated_path[0].y = 1
				if transform.origin.distance_to(calculated_path[0]) > 2:
					var moving_direction = transform.origin.direction_to(calculated_path[0])
					move_and_slide(moving_direction * speed)
					look_at(transform.origin - moving_direction, Vector3(0, 1, 0))
				else:
					calculated_path.remove(0)
			else:
				calculated_path = _nav.get_simple_path(transform.origin, closest_hostage.get_global_transform().origin)
	else:
		hunting = false

func get_closest_hostage():
	var min_distance = 99999;
	closest_hostage = null
	for hostage in hostages:
		var distance = get_global_transform().origin.distance_to(hostage.get_global_transform().origin)
		if min_distance > distance:
			closest_hostage = hostage
			min_distance = distance
	return closest_hostage

func _on_GrabArea_body_entered(body):
	if body.is_in_group('hostages'):
		get_node("/root/Prototype/GameManager").dead_hostages += 1
		body.queue_free()

func _on_SenseArea_body_entered(body):
	if body.is_in_group('hostages'):
		hostages.append(body)

func _on_SenseArea_body_exited(body):
	if body.is_in_group('hostages'):
		hostages.erase(body)