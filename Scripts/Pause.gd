extends Control

func _process(delta):
	if Input.is_action_just_pressed('ui_cancel'):
		self.visible = !self.visible
		if get_node('/root/Prototype/GameManager').game_started:
			get_tree().paused = !get_tree().paused

func _on_ContinueButton_pressed():
	self.visible = false
	if get_node('/root/Prototype/GameManager').game_started:
			get_tree().paused = false
